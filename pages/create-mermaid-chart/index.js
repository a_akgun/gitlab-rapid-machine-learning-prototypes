import Head from "next/head";
import { useState, useEffect } from "react";
import styles from "./index.module.css";


export default function Explain() {
  const defaultInput = `A follows B. C follows B. D follows B.`;
  const [mrInput, setMrInput] = useState(defaultInput);
  const [result, setResult] = useState('');

  async function onSubmit(event) {
    event.preventDefault();
    try {
      const response = await fetch("/api/mermaid", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ mr: mrInput }),
      });

      const data = await response.json();
      if (response.status !== 200) {
        throw data.error || new Error(`Request failed with status ${response.status}`);
      }
      setResult(data.result);
    } catch(error) {
      console.error(error);
      alert(error.message);
    }
  }

  return (
    <div>
      <Head>
        <title>Gitlab AI Assisted - Create mermaid chart</title>
      </Head>
      <main className={styles.main}>
        <a href="/">
          <img src="/tanuki.svg" className={styles.icon} />
        </a>
        <h3>Create mermaid chart</h3>
        <form onSubmit={onSubmit}>
          <textarea
            name="mr"
            placeholder="Enter your code"
            rows = "5"
            value={mrInput}
            onChange={(e) => setMrInput(e.target.value)}
          />
          <input type="submit" value="Go" />
        </form>
      <div className={styles.result}>
        <pre>{result}</pre>
      </div>
      </main>
      </div>
  )
}
