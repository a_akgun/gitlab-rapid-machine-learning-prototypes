# GitLab Rapid Machine Learning Prototypes - prompts


```
git clone https://gitlab.com/a_akgun/gitlab-rapid-machine-learning-prototypes.git

cd gitlab-rapid-machine-learning-prototypes

# Enter your open ai credentials in .env file
# OPENAI_API_KEY=XXX
npm i
npm run dev
```

Now go to http://localhost:3000/